import os

def continuar():
    print()
    input('Presione una tecla para continuar ...')
    os.system('cls')

def menu():
    print('1) Cargar Productos')
    print('2) Mostrar Listado de Productos')
    print('3) Mostrar los productos cuyo stock se encuentre en el intervalo [desde, hasta]')
    print('4) Proceso que le sume X al stock cuyo valor actual de stock sea menor al valor Y ')
    print('5) Eliminar todos los productos cuyo stock sea igual a cero')
    print('6) Salir')
    eleccion = int(input('Elija una Opción: '))
    while not((eleccion >= 1) and (eleccion <= 6)):
        eleccion = int(input('Elija una Opción: '))
    os.system('cls')
    return eleccion

def leerPrecio():
    precio = float(input('Precio: '))
    while not(precio > 0):
        precio = float(input('Precio: '))
    return precio

def leerStock():
    stock = int(input('Stock: '))
    while not(stock > 0):
        stock = int(input('Stock: '))
    return stock

def mostrar(diccionario):
    print('Listado de Productos')
    for clave, valor in diccionario.items():
        print(clave,valor)

def leerProductos():
    print('Cargar Lista de Productos')
    producto = {100:['Arroz', 10.0,1],
                101:['Azucar', 5.0,0],
                102:['Fideo', 8.5,0],
                103:['Aceite', 6.5,10],
                104:['Polenta', 3.0,18],
                105:['Cafe', 4.0,2],
                106:['Yerba', 7.5,9],
                107:['Sal',2.5,12]}
    codigo = -1
    while (codigo != 0):
        codigo = int(input('CODIGO (cero para finalizar): '))
        if codigo != 0: 
            if codigo not in producto:    
                nombre = input('DESCRIPCION: ')
                precio=leerPrecio()
                stock = leerStock()
                producto[codigo] = [nombre,precio,stock]
                print('Agregado Correctamente')
            else:
                print('El Producto ya existe')
    return producto

def mostrarStock(producto):
    print('Lista de Productos Desde el Stock: ')
    stock1 = leerStock()
    print('Lista de Productos hasta el Stock: ')
    stock2 = leerStock()
    for codigo, datos in producto.items():
        if ((datos[2] >= stock1) and (datos[2] <= stock2)): 
            print(codigo, datos)    

def modificarStock(producto):
    print("Modificar Productos en Stock")
    print("Ingrese Stock X: ")
    x = leerStock()
    print("Ingrese Stock Y: ")
    y = leerStock()
    for cod, datos in producto.items():
        if (datos[2]<y):
            datos[2]=datos[2]+x
            print("Modificado Corectamente")

def eliminar(producto):
    for item in list(producto.keys()):
        if producto [item][2]==0:
            del producto[item]
            print("Eliminado Correctamente")


#principal
opcion = 0
os.system('cls')
while (opcion != 7):
    opcion = menu()
    if opcion == 1:
        producto = leerProductos()
    elif opcion == 2:
        mostrar(producto)
    elif opcion == 3:
        mostrarStock(producto)
    elif opcion == 4:
        modificarStock(producto)
    elif opcion == 5:
        eliminar(producto)
    elif (opcion == 6):        
        print('Fin del programa')
    continuar()
